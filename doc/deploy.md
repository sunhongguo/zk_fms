﻿
# 编译部署文档

#### 一. JAVA的编译和部署     
#### 1.编译    
开发工具：eclipse      
编译jar包过程：
###### a.选择项目右击-> Run As->Maven Clean
  清除之前编译的数据
###### b.选择项目右击-> Run As->Maven Install
  在项目的文件夹下，target文件夹中生成一个新的jar文件
        
#### 2.部署     
环境：linux系统，nginx服务器，Mysql数据库，JDK1.8
##### a. MySql数据库
 - 使用ps -ef | grep mysql查看数据库安装情况；没有则需要安装mysql数据库，如果已经安装，需要设置用户:root,密码：12345678；
 - 创建名称为nanjing_data数据库：create DATABASE nanjing_data;
 - 使用：use nanjing_data
 - 导入sql文件：source /home/user/data/statistic.sql;其中statistic.sql文件为已经生成的包含所有数据表的文件，上述命令的后半部分是sql文件在linux中的目录
##### b.在本地创建新的文件夹，并将之前编译好的jar复制过来
##### c.进入上述文件夹目录下，使用命令运行jar文件：nohup java -jar xxx.jar &

#### 二.nginx部署     
##### 1. 安装依赖包:yum -y install gcc zlib zlib-devel pcre-devel openssl openssl-devel        
##### 2.下载并解压安装包
  wget http://nginx.org/download/nginx-1.13.7.tar.gz    
  tar -xvf nginx-1.13.7.tar.gz    
##### 3.安装nginx
  执行命令: ./configure     
  执行make命令: make     
  执行make install命令: make install
##### 4.配置nginx.conf
  进入nginx解压后的目录，打开配置文件
  vi /conf/nginx.conf
  修改server中内容：
```python
        listen       38222;
        server_name  112.80.35.9;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
            root   html;
            index  index.html index.htm;
        }

       location /fms {
                proxy_read_timeout 300; # https://github.com/gitlabhq/gitlabhq/issues/694
                proxy_connect_timeout 300; # https://github.com/gitlabhq/gitlabhq/issues/694
                proxy_redirect     off;
                proxy_set_header   Host             $http_host;
                proxy_set_header   X-Forward-For    $remote_addr;
                proxy_set_header   X-Real-IP        $remote_addr;
                proxy_pass http://127.0.0.1:8080;
        }
```
##### 5.启动nginx
  ./nginx 启动    
  ./nginx -s stop 关闭     
  ./nginx -s reload 重启



#### 二.Vue编译与部署
	1.安装vue开发环境：npm install vue
	2.进入项目根目录：cd fms_front
	3.安装项目依赖包：npm run install
	4.执行编译：npm run build
	5.编译完之后，在项目根目录中生成dist文件夹
	6.进入dist文件夹，将文件夹内所有文件复制到服务器->nginx->html文件夹中


