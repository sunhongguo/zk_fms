﻿
#API文档

## 一 数据库表结构
#### 1.工业总产值表（单位：亿元）db_total_industrial_output_value
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   city |  String | 城市名      |  产值对应的城市
   year |  int   | 年份       | 产值对应的年份
   value |  double | 数值     | 具体的数值（单位：亿元）
   
#### 2.南京市规模以上工业企业主营业务收入（单位：亿元）db_main_business_income_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   totalize |  double | 累计值     | 具体的数值（单位：亿元）
   average |  double | 平均值
   income |  double | 主营业务收入
   rate |  double | 同比增长率  | 单位：百分比%
   
#### 3.南京市工业增加值表（单位：亿元）db_industrial_value_added_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   value |  double | 工业增加值     | 具体的数值（单位：亿元）
   
#### 4.南京市工业固定资产投资额表（单位：亿元）db_industrial_fixed_asset_investment_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   value |  double | 投资额     | 具体的数值（单位：亿元）
   
#### 5.南京市利润总额（单位：亿元）db_total_profit_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   value |  double | 利润值     | 具体的数值（单位：亿元）
   
#### 6.南京税金总额（单位：亿元）db_total_taxation_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   value |  double | 税金     | 具体的数值（单位：亿元）
   
#### 7.第二产业从业人员（万人）db_employee_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   number |  double | 从业人员（万人） 
   rate |  double | 同比增长率  | 单位：百分比%

#### 8.南京市创新能力表db_innovation_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   patent |  int   | 有效发明专利数       | 件
   input_value |  double | 工业企业研发机构仪器和设备投入额（单位：亿元）
   input_rate |  double | 工业企业研发机构仪器和设备投入额增速(%) | 单位：百分比%
   
#### 9.南京市政府专项支持资金( 单位：亿元）db_government_special_funds_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   value |  double | 政府基金预算支出（单位：亿元）
   rate |  double | 同比增长率  | 单位：百分比%
   
#### 10.南京市节能环保db_ecosaving_data_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   coal_value |  double | 单位工业增加值能耗-煤 |  单位：吨标煤/万元
   coal_rate |  double | 同比增长率-煤  | 单位：百分比%
   electricity_value |  double | 全社会用电量 |  单位：亿千瓦时
   electricity_rate |  double | 用电量同比增长率  | 单位：百分比%
   water_value |  double | 单位工业增加值用水量（立方米/万元）
   water_repeat |  double | 重复用水总量（立方米）
   water_rate |  double | 用水量同比增长率  | 单位：百分比%

#### 11.各产业全国经济贡献率 db_contribution_rate
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   value |  double | 贡献率
   city |  String | 所在地
   
#### 12.城市工业产业总产值（单位：亿元）db_region_industrial_output_value_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   value |  double | 总产值（单位：亿元）
   region |  String | 区域所在地
   
#### 13.示范基地表db_demonstration_base
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id     |  Long   |  id      
   city   |  String   |   城市       | 基地所在城市
   industry_type |  String |行业类型
   industry_details |  String | 行业类型细分
   address |  String | 位置
   
#### 14.南京市高技术产业总产值（单位：亿元）db_total_hitech_output_value_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id     |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   month |  int   | 月       
   day  |  int   |  日      
   totalize |  double | 累计值  | 单位：亿元
   value |  double | 总产值 | 单位：亿元
   rate |  double  | 同比增长率%
   
#### 15.南京市各产业综合能耗（单位：亿元）db_comprehensive_energy_consumption_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id     |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   month |  int   | 月       
   day  |  int   |  日      
   totalize |  double | 综合能耗:累计值 | 单位：亿元
   value |  double | 综合能耗:累计值 | 单位：亿元
   rate |  double  | 同比增长率%
   
#### 16.区域总产值---销售收入（单位：亿元）db_region_output_value_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   value |  double | 总产值（单位：亿元）
   region |  String | 区域所在地
   
#### 17.大中型企业 db_enterprise_largeandmedium_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   month |  int   | 月份 
   day |  int   | 天
   totalize |  double | 累计值 单位：亿元
   rate  |  double | 累计同比增长%
   number | int | 企业数量
   
#### 18.国有及国有控股企业 db_enterprise_state_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   month |  int   | 月份 
   day |  int   | 天
   totalize |  double | 累计值 单位：亿元
   rate  |  double | 累计同比增长%
   
#### 19.私营企业 db_enterprise_private_nj
   参数名 | 类型  | 描述  | 备注  
   :----: | :----:| :---: | :---: 
   id       |  Long   |  id      
   year |  int   | 年份       | 产值对应的年份
   month |  int   | 月份 
   day |  int   | 天
   totalize |  double | 累计值 单位：亿元
   rate  |  double | 累计同比增长%
   
## 二 接口
#### 1.获取工业总产值
#### 接口地址URL
GET   /fms/tiov/querylist
#### 请求参数说明
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  city  | String   | 城市名称  | 非必须
  year  | int   | 年份    | 必须，没有具体年份时year=0
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/tiov/querylist?year=0
```
#### 返回参数
```python
返回正确
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "id": 1,
            "city": "南京市",
            "year": 2015,
            "value": 13065.8
        },
        {
            "id": 2,
            "city": "南京市",
            "year": 2016,
            "value": 13026.9
        },
        {
            "id": 3,
            "city": "南京市",
            "year": 2017,
            "value": 13759.32
        }
    ]
}
```

#### 2.总体情况
#### 接口地址URL
GET   /fms/overall/query
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/overall/query
```
#### 返回参数
```python
返回正确
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "name": "主营业务收入",
            "object": [
                {
                    "id": 1,
                    "year": 2015,
                    "totalize": 11022.76,
                    "average": 918.56,
                    "income": 11022.76,
                    "rate": -5.3
                },
                {
                    "id": 2,
                    "year": 2016,
                    "totalize": 12421.68,
                    "average": 1035.14,
                    "income": 12421.68,
                    "rate": 2.2
                },
                {
                    "id": 3,
                    "year": 2017,
                    "totalize": 10777.32,
                    "average": 979.76,
                    "income": 11757.08,
                    "rate": -5.35
                }
            ]
        },
        {
            "name": "工业总产值",
            "object": [
                {
                    "id": 1,
                    "city": "南京市",
                    "year": 2015,
                    "value": 13065.8
                },
                {
                    "id": 2,
                    "city": "南京市",
                    "year": 2016,
                    "value": 13026.9
                },
                {
                    "id": 3,
                    "city": "南京市",
                    "year": 2017,
                    "value": 13759.32
                }
            ]
        },
        {
            "name": "工业增加值",
            "object": [
                {
                    "id": 1,
                    "year": 2015,
                    "value": 3043.5
                },
                {
                    "id": 2,
                    "year": 2016,
                    "value": 3050.55
                },
                {
                    "id": 3,
                    "year": 2017,
                    "value": 3363.37
                }
            ]
        },
        {
            "name": "工业固定资产投资额",
            "object": [
                {
                    "id": 1,
                    "year": 2015,
                    "value": 2283.31
                },
                {
                    "id": 2,
                    "year": 2016,
                    "value": 1784.22
                },
                {
                    "id": 3,
                    "year": 2017,
                    "value": 2093.03
                }
            ]
        }
    ]
}
```


#### 3.获取南京市创新能力数据
#### 接口地址URL
GET   /fms/innovation/querylist
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/innovation/querylist
```
#### 返回参数
```python
返回正确
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "id": 1,
            "year": 2018,
            "patent": 44089,
            "input_value": 162.62,
            "input_rate": 6.8
        }
    ]
}
```

#### 4.获取南京市政府专项支持资金
#### 接口地址URL
GET   /fms/specialfunds/querylist
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/specialfunds/querylist
```
#### 返回参数
```python
返回正确
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "id": 1,
            "year": 2015,
            "value": 880.7,
            "rate": -5.08
        },
        {
            "id": 2,
            "year": 2016,
            "value": 1479.3,
            "rate": 67.97
        },
        {
            "id": 3,
            "year": 2017,
            "value": 1612,
            "rate": 8.97
        },
        {
            "id": 4,
            "year": 2018,
            "value": 1589.9,
            "rate": -1.37
        }
    ]
}
```

#### 5.获取南京市节能环保
#### 接口地址URL
GET   /fms/ecosaving/query
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/ecosaving/query
```
#### 返回参数
```python
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "name": "能耗数值",
            "object": [
                {
                    "year": 2018,
                    "coal_value": 0,
                    "electricity_value": 331.27,
                    "waterValue": 0
                },
                {
                    "year": 2017,
                    "coal_value": 3784.38,
                    "electricity_value": 318.14,
                    "waterValue": 15.75
                },
                {
                    "year": 2016,
                    "coal_value": 3822.21,
                    "electricity_value": 305.38,
                    "waterValue": 15.48
                }
            ]
        },
        {
            "name": "能耗增长率",
            "object": [
                {
                    "year": 2018,
                    "coal_rate": 0,
                    "electricity_rate": 3.96,
                    "water_rate": 0
                },
                {
                    "year": 2017,
                    "coal_rate": -1.2,
                    "electricity_rate": 4.01,
                    "water_rate": 1.71
                },
                {
                    "year": 2016,
                    "coal_rate": 3.95,
                    "electricity_rate": 0,
                    "water_rate": 1.68
                }
            ]
        }
    ]
}
```

#### 6.获取南京市人力资源信息
#### 接口地址URL
GET   /fms/hr/query
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/hr/query
```
#### 返回参数
```python
返回正确
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "name": "主营业务收入",
            "object": [
                {
                    "id": 1,
                    "year": 2015,
                    "totalize": 11022.76,
                    "average": 918.56,
                    "income": 11022.76,
                    "rate": -5.3
                },
                {
                    "id": 2,
                    "year": 2016,
                    "totalize": 12421.68,
                    "average": 1035.14,
                    "income": 12421.68,
                    "rate": 2.2
                },
                {
                    "id": 3,
                    "year": 2017,
                    "totalize": 10777.32,
                    "average": 979.76,
                    "income": 11757.08,
                    "rate": -5.35
                }
            ]
        },
        {
            "name": "利润总额",
            "object": [
                {
                    "id": 1,
                    "year": 2015,
                    "value": 741.55
                },
                {
                    "id": 2,
                    "year": 2016,
                    "value": 941.81
                },
                {
                    "id": 3,
                    "year": 2017,
                    "value": 860
                }
            ]
        },
        {
            "name": "从业人员增长率",
            "object": [
                {
                    "id": 2,
                    "year": 2015,
                    "number": 98.82,
                    "rate": -0.138898571
                },
                {
                    "id": 3,
                    "year": 2016,
                    "number": 90.68,
                    "rate": -0.082371989
                },
                {
                    "id": 4,
                    "year": 2017,
                    "number": 90.62,
                    "rate": -0.000661667
                }
            ]
        }
    ]
}
```

#### 7.获取南京市经济效益
#### 接口地址URL
GET   /fms/economic/query
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/economic/query
```
#### 返回参数
```python
返回正确
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "name": "利润总额",
            "object": [
                {
                    "id": 1,
                    "year": 2015,
                    "value": 741.55
                },
                {
                    "id": 2,
                    "year": 2016,
                    "value": 941.81
                },
                {
                    "id": 3,
                    "year": 2017,
                    "value": 860
                }
            ]
        },
        {
            "name": "税金总额",
            "object": [
                {
                    "id": 1,
                    "year": 2015,
                    "value": 423.14
                },
                {
                    "id": 2,
                    "year": 2016,
                    "value": 329.91
                },
                {
                    "id": 3,
                    "year": 2017,
                    "value": 329.91
                }
            ]
        }
    ]
}
```

#### 8.获取经济贡献率
#### 接口地址URL
GET   /fms/contributionrate/query
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/contributionrate/query
```
#### 返回参数
```python
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "name": "2017",
            "object": [
                {
                    "id": 3,
                    "city": "南京市",
                    "year": 2017,
                    "value": 38.03
                },
               ....
                {
                    "id": 39,
                    "city": "镇江市",
                    "year": 2017,
                    "value": 49.32
                }
            ]
        },
        {
            "name": "2016",
            "object": [
                {
                    "id": 2,
                    "city": "南京市",
                    "year": 2016,
                    "value": 39.2
                },
               ....
                {
                    "id": 38,
                    "city": "镇江市",
                    "year": 2016,
                    "value": 48.8
                }
            ]
        },
        {
            "name": "2015",
            "object": [
                {
                    "id": 1,
                    "city": "南京市",
                    "year": 2015,
                    "value": 40.3
                },
                .....
                {
                    "id": 37,
                    "city": "镇江市",
                    "year": 2015,
                    "value": 49.3
                }
            ]
        }
    ]
}
```

#### 9.城市工业产业总产值:11个区的工业总产值
#### 接口地址URL
GET   /fms/riov/query
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/riov/query
```
#### 返回参数
```python
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "id": 1,
            "region": "江宁区",
            "year": 2017,
            "value": 3252.28
        },
        {
            "id": 2,
            "region": "鼓楼区",
            "year": 2017,
            "value": 150.22
        },
        {
            "id": 3,
            "region": "栖霞区",
            "year": 2017,
            "value": 2718.91
        },
        {
            "id": 4,
            "region": "秦淮区",
            "year": 2017,
            "value": 191.91
        },
        {
            "id": 5,
            "region": "玄武区",
            "year": 2017,
            "value": 42.28
        },
        {
            "id": 6,
            "region": "溧水区",
            "year": 2017,
            "value": 1091.25
        },
        {
            "id": 7,
            "region": "高淳区",
            "year": 2017,
            "value": 944.43
        },
        {
            "id": 8,
            "region": "建邺区",
            "year": 2017,
            "value": 4.25
        },
        {
            "id": 9,
            "region": "六合区",
            "year": 2017,
            "value": 1681.11
        },
        {
            "id": 10,
            "region": "浦口区",
            "year": 2017,
            "value": 1569.52
        }
    ]
}
```

#### 10.示范基地表
#### 接口地址URL
GET   /fms/demonbase/query
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/demonbase/query
```
#### 返回参数
```python
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "id": 1,
            "city": "无锡",
            "industry_type": "电子信息",
            "industry_details": "电子信息（传感网）",
            "address": "江苏无锡高新技术产业开发区"
        },
        {
            "id": 2,
            "city": "苏州",
            "industry_type": "电子信息",
            "industry_details": "电子信息",
            "address": "江苏苏州工业园区"
        },
     .......
        {
            "id": 24,
            "city": "南通",
            "industry_type": "大型数据中心",
            "industry_details": "大型数据中心（实时应用类）",
            "address": "江苏南通国际数据中心产业园"
        }
    ]
}
```

#### 11.南京市高技术产业总产值
#### 接口地址URL
GET   /fms/hitech/query
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/hitech/query
```
#### 返回参数
```python
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "id": 1,
            "year": 2015,
            "month": 12,
            "day": 31,
            "value": 5719.94,
            "totalize": 5719.94,
            "rate": -0.9
        },
        {
            "id": 2,
            "year": 2016,
            "month": 12,
            "day": 31,
            "value": 3063.39,
            "totalize": 3063.39,
            "rate": 2.9
        },
        {
            "id": 3,
            "year": 2017,
            "month": 11,
            "day": 30,
            "value": 3286.77,
            "totalize": 3012.87,
            "rate": 8.3
        }
    ]
}
```

#### 12.南京市各产业综合能耗
#### 接口地址URL
GET   /fms/energyconsumption/query
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/energyconsumption/query
```
#### 返回参数
```python
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "id": 1,
            "year": 2015,
            "month": 12,
            "day": 31,
            "value": 3670.95,
            "totalize": 3670.95,
            "rate": 0.9
        },
        {
            "id": 2,
            "year": 2016,
            "month": 11,
            "day": 30,
            "value": 3824.37,
            "totalize": 3505.67,
            "rate": 5.1
        },
        {
            "id": 3,
            "year": 2017,
            "month": 12,
            "day": 31,
            "value": 3784.38,
            "totalize": 3784.38,
            "rate": -1.2
        },
        {
            "id": 4,
            "year": 2018,
            "month": 12,
            "day": 31,
            "value": 3826.97,
            "totalize": 3826.97,
            "rate": 0.7
        }
    ]
}
```

#### 13.区域总产值:销售收入
#### 接口地址URL
GET   /fms/regionoutput/query
#### 请求参数说明
   参数名 |  类型  | 描述   |  备注  
  :--------: | :-----:  | :----:| :-----: 
  year  | int   | 年份    | 必须，没有具体年份时year=0
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/regionoutput/query?year=2017
```
#### 返回参数
```python
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "id": 3,
            "region": "江宁区",
            "year": 2017,
            "value": 3252.28
        },
        {
            "id": 6,
            "region": "鼓楼区",
            "year": 2017,
            "value": 150.22
        },
        {
            "id": 9,
            "region": "栖霞区",
            "year": 2017,
            "value": 2718.91
        },
        {
            "id": 12,
            "region": "秦淮区",
            "year": 2017,
            "value": 191.91
        },
        {
            "id": 15,
            "region": "玄武区",
            "year": 2017,
            "value": 42.28
        },
        {
            "id": 18,
            "region": "溧水区",
            "year": 2017,
            "value": 1091.25
        },
        {
            "id": 21,
            "region": "高淳区",
            "year": 2017,
            "value": 944.43
        },
        {
            "id": 24,
            "region": "建邺区",
            "year": 2017,
            "value": 4.25
        },
        {
            "id": 27,
            "region": "六合区",
            "year": 2017,
            "value": 1681.11
        },
        {
            "id": 30,
            "region": "浦口区",
            "year": 2017,
            "value": 1569.52
        }
    ]
}
```

#### 14.大中型企业
#### 接口地址URL
GET   /fms/enterprise_lm/query
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/enterprise_lm/query
```
#### 返回参数
```python
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "id": 1,
            "year": 2017,
            "month": 12,
            "day": 31,
            "totalize": 8575.48,
            "rate": 11.89,
            "number": 255
        },
        {
            "id": 2,
            "year": 2016,
            "month": 12,
            "day": 31,
            "totalize": 8875.42,
            "rate": 1.1,
            "number": 512
        },
        {
            "id": 3,
            "year": 2015,
            "month": 12,
            "day": 31,
            "totalize": 8980.37,
            "rate": -3.5,
            "number": 0
        }
    ]
}
```

#### 15.国有及国有控股企业
#### 接口地址URL
GET   /fms/enterprise_state/query
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/enterprise_state/query
```
#### 返回参数
```python
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "id": 1,
            "year": 2017,
            "month": 12,
            "day": 31,
            "totalize": 4989322,
            "rate": 12.1
        },
        {
            "id": 2,
            "year": 2016,
            "month": 12,
            "day": 31,
            "totalize": 4053.55,
            "rate": 0.8
        },
        {
            "id": 3,
            "year": 2015,
            "month": 12,
            "day": 31,
            "totalize": 3993.79,
            "rate": -9.9
        }
    ]
}
```

#### 16.私营企业
#### 接口地址URL
GET   /fms/enterprise_private/query
#### 请求参数说明
   无
#### 请求示例
```python
get  http://10.10.4.222:8080/fms/enterprise_private/query
```
#### 返回参数
```python
{
    "code": 0,
    "message": "获取成功！",
    "object": [
        {
            "id": 1,
            "year": 2017,
            "month": 12,
            "day": 31,
            "totalize": 2106.82,
            "rate": 4.3
        },
        {
            "id": 2,
            "year": 2016,
            "month": 12,
            "day": 31,
            "totalize": 2392.13,
            "rate": 1.2
        },
        {
            "id": 3,
            "year": 2015,
            "month": 12,
            "day": 31,
            "totalize": 2360.87,
            "rate": 3.7
        }
    ]
}
```




