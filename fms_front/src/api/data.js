import axios from '@/util/request'


export const getGrossIndustrialOutput = () => {
	return axios.request({
	  url:'fms/tiov/querylist',
		params:{
			year:2017
		},
	  method: 'get'
	})
}

///////区域分子

//区域销售收入
export const areaIncomeList = (year) => {
    return axios.request({
        url: 'fms/regionoutput/query',
        params: {
            year
        },
        method: 'get'
    })
}

//大中型企业
export const largeandmediumEnterprises = () => {
    return axios.request({
        url: 'fms/enterprise_lm/query',
        method: 'get'
    })
}


//国有及国有控股企业
export const stateOwnedorHoldingEnterprises = () => {
    return axios.request({
        url: 'fms/enterprise_state/query',
        method: 'get'
    })
}

//私营企业
export const privateEnterprise = () => {
    return axios.request({
        url: 'fms/enterprise_private/query',
        method: 'get'
    })
}


export const getOverallSituationData = () => {
	return axios.request({
		url:'fms/overall/query',
		method:'get'
	})
}

export const getInnovationAbilityData = () => {
	return axios.request({
		url:'fms/innovation/querylist',
		method:'get'
	})
}


export const getGovernmenSupportData = () => {
	return axios.request({
		url:'fms/specialfunds/querylist',
		method:'get'
	})
}

export const getHumanResourceData = () => {
	return axios.request({
		url:'fms/hr/query',
		method:'get'
	})
}

export const getEconomicPerformanceData = () => {
	return axios.request({
		url:'fms/economic/query',
		method:'get'
	})
}

export const getEnvironmentalProtectionData = () => {
	return axios.request({
		url:'fms/ecosaving/query',
		method:'get'
	})
}

export const getDemonstrationBaseData = () => {
	return axios.request({
		url:'fms/demonbase/query',
		method:'get'
	})
}

export const getRossIndustrialOutputData = () => {
	return axios.request({
		url:'fms/riov/query',
		method:'get'
	})
}

export const getEconomicContributionData = () => {
	return axios.request({
		url:'fms/contributionrate/query',
		method:'get'
	})
}


export const getHighTechnologyData = () => {
	return axios.request({
		url:'fms/hitech/query',
		method:'get'
	})
}

export const getComprehensiveEnergyConsumptionData = () => {
	return axios.request({
		url:'fms/energyconsumption/query',
		method:'get'
	})
}
