import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/home/home.vue'),
      children: [
      	{
      	  path: 'industry',
      	  name: 'industry',
      	  meta: {
      	    title: '工业经济'
      	  },
      	  component: () => import('@/views/industry/industry.vue'),
      	},
				{
				  path: 'estate',
				  name: 'estate',
				  meta: {
				    title: '产业分析'
				  },
				  component: () => import('@/views/estate/estate.vue')
				},
				{
				  path: 'area',
				  name: 'area',
				  meta: {
				    title: '区域分析'
				  },
				  component: () => import('@/views/area/area.vue')
				},
          {
              path: 'industrySituation',
              name: 'industrySituation',
              meta: {
                  title: '工业经济总体情况'
              },
              component: () => import('@/views/overallSituation/overallSituation.vue')
          }
			]
		}
  ]
})
