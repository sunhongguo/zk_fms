export default {
  /**
   * @description 配置显示在浏览器标签的title
   */
  title: '工业经济可持续发展监测系统',

  /**
   * @description api请求基础路径
   */
  baseUrl: {
    dev: 'http://112.80.35.9:38222/',
    pro: 'http://112.80.35.9:38222/'
  },
	
  /**
   * @description 默认打开的首页的路由name值，默认为home
   */
  homeName: 'home',

}
