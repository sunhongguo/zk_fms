import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
	  barColorList:['#fffff','#00000'],
	},
  mutations: {
		setBarColorList (state, barColorList) {
			state.barColorList = barColorList
		}
  },
	getters:{
		 getBarColorList: state => state.barColorList
	},
  actions: {
		setBarColorList ({ commit }, { barColorList }) {
			commit('setBarColorList', data.token)
		}
  }
})
