export const CURRENT_PAGER = 'current_pager'

export const setCurrentPager = (pager) => {
	console.log("------->",pager)
		localStorage.setItem(CURRENT_PAGER,pager)
}

export const getCurrentPager = () => {
	var pager = localStorage.getItem(CURRENT_PAGER);
	if(pager == null){
		return 0;
	}
	return pager;
}
