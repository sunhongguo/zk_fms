package com.nanjing.statistics.model;

import java.io.Serializable;

// 南京市高技术产业（单位：亿元）
public class TotalHiTechOutputValue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id; // ID
	private int year; // 年份
	private int month; // 月
	private int day; // 日
	private double value; // 总产值 单位：亿元
	private double totalize; // 累计值 单位：亿元
	private double rate; // 同比增长率 %

	public TotalHiTechOutputValue() {
		super();
	}

	public TotalHiTechOutputValue(int year, int month, int day, double value, double totalize, double rate) {
		this.year = year;
		this.month = month;
		this.day = day;
		this.value = value;
		this.totalize = totalize;
		this.rate = rate;
	}

	// id
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	// year
	public void setYear(int year) {
		this.year = year;
	}

	public int getYear() {
		return year;
	}

	// month
	public void setMonth(int month) {
		this.month = month;
	}

	public int getMonth() {
		return month;
	}

	// day
	public void setDay(int day) {
		this.day = day;
	}

	public int getDay() {
		return day;
	}

	// value
	public void setValue(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	// totalize
	public void setTotalize(double totalize) {
		this.totalize = totalize;
	}

	public double getTotalize() {
		return totalize;
	}

	// rate
	public void setRate(double rate) {
		this.rate = rate;
	}

	public double getRate() {
		return rate;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "year: " + this.year + ",month: " + this.month + ",day: " + this.day
				+ ",value: " + this.value
				+ ",totalize: " + this.totalize
				+ ",rate: " + this.rate;
	}
}
