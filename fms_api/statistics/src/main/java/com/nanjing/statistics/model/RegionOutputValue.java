package com.nanjing.statistics.model;

import java.io.Serializable;

// 区域总产值（单位：亿元）
public class RegionOutputValue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id; // ID
	private String region; // 区域
	private int year; // 年份
	private double value; // 总产值（单位：亿元）

	public RegionOutputValue() {
		super();
	}

	public RegionOutputValue(String region, int year, double value) {
		this.region = region;
		this.year = year;
		this.value = value;
	}

	// id
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	// region
	public void setRegion(String region) {
		this.region = region;
	}

	public String getRegion() {
		return region;
	}

	// year
	public void setYear(int year) {
		this.year = year;
	}

	public int getYear() {
		return year;
	}

	// value
	public void setValue(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "region: " + this.region + ", year: " + this.year + ",value: " + this.value;
	}
}
