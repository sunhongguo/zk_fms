package com.nanjing.statistics.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.nanjing.statistics.model.EnterpriseLargeandMedium;
import com.nanjing.statistics.model.EnterprisePrivate;
import com.nanjing.statistics.model.EnterpriseState;
import com.nanjing.statistics.model.RegionOutputValue;

//区域分析相关数据查询
@Mapper
public interface RegionAnalysisMapper {

	// 区域总产值：销售收入
	@Select("SELECT * FROM db_region_output_value_nj")
	List<RegionOutputValue> getRegionOutputValueList();

	// 根据年份获取区域总产值
	@Select("SELECT * FROM db_region_output_value_nj WHERE year = #{year}")
	List<RegionOutputValue> getRegionOutputValueListByYear(int year);

	// 大中型企业
	@Select("SELECT * FROM db_enterprise_largeandmedium_nj")
	List<EnterpriseLargeandMedium> getEnterpriseLM();

	// 国有及国有控股企业
	@Select("SELECT * FROM db_enterprise_state_nj")
	List<EnterpriseState> getEnterpriseState();

	// 私营企业
	@Select("SELECT * FROM db_enterprise_private_nj")
	List<EnterprisePrivate> getEnterprisePrivate();
}
