package com.nanjing.statistics.model;

import java.io.Serializable;

// 规模以上工业企业主营业务收入
public class MainBusinessIncome implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id; // ID
	private int year; // 年份
	private double totalize; // 累计值
	private double average; // 平均值
	private double income; // 主营业务收入
	private double rate; // 增长率

	public MainBusinessIncome() {
		super();
	}

	public MainBusinessIncome(int year, double totalize, double average, double income, double rate) {
		this.year = year;
		this.totalize = totalize;
		this.average = average;
		this.income = income;
		this.rate = rate;
	}

	// id
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	// year
	public void setYear(int year) {
		this.year = year;
	}

	public int getYear() {
		return year;
	}

	// totalize
	public void setTotalize(double totalize) {
		this.totalize = totalize;
	}

	public double getTotalize() {
		return totalize;
	}

	// average
	public void setAverage(double average) {
		this.average = average;
	}

	public double getAverage() {
		return average;
	}

	// income
	public void setIncome(double income) {
		this.income = income;
	}

	public double getIncome() {
		return income;
	}

	// rate
	public void setRate(double rate) {
		this.rate = rate;
	}

	public double getRate() {
		return rate;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "year: " + this.year + ", totalize: " + this.totalize + ", average: " + this.average + ", income: "
				+ this.income + ",rate: " + this.rate;
	}
}
