package com.nanjing.statistics.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.nanjing.statistics.model.ComprehensiveEnergyConsumption;
import com.nanjing.statistics.model.ContributionRate;
import com.nanjing.statistics.model.DemonstrationBase;
import com.nanjing.statistics.model.RegionIndustrialOutputValue;
import com.nanjing.statistics.model.TotalHiTechOutputValue;

// 产业分析相关数据查询
@Mapper
public interface IndustrialAnalysisMapper {

	// 各产业全国经济贡献率
	@Select("SELECT * FROM db_contribution_rate")
	List<ContributionRate> getContributionRateList();
	
	// 各产业全国经济贡献率:获取年份
	@Select("SELECT DISTINCT year FROM db_contribution_rate ORDER BY year DESC")
	List<Integer> getContributionRateYear();
	
	// 各产业全国经济贡献率：根据年份获取列表
	@Select("SELECT * FROM db_contribution_rate WHERE year = #{year}")
	List<ContributionRate> getContributionRateListByYear(int year);

	// 城市工业产业总产值
	@Select("SELECT * FROM db_region_industrial_output_value_nj ORDER BY value DESC")
	List<RegionIndustrialOutputValue> getRegionIndustrialOutputValueList();

	// 示范基地表
	@Select("SELECT * FROM db_demonstration_base")
	List<DemonstrationBase> getDemonstrationBaseList();
	
	// 南京市高技术产业
	@Select("SELECT * FROM db_total_hitech_output_value_nj")
	List<TotalHiTechOutputValue> getHiTechOutputValueList();

	// 南京市各产业综合能耗（单位：亿元）
	@Select("SELECT * FROM db_comprehensive_energy_consumption_nj")
	List<ComprehensiveEnergyConsumption> getComEnergyConsumptionList();
}
