package com.nanjing.statistics.model;

import java.io.Serializable;

// 示范基地表
public class DemonstrationBase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id; // ID
	private String city; // 城市
	private String industry_type;  // 基地所在行业类型
	private String industry_details; // 行业类型细分
	private String address;    // 基地所在位置

	public DemonstrationBase() {
		super();
	}

	public DemonstrationBase(String city, String industry_type, String industry_details, String address) {
		this.city = city;
		this.industry_type = industry_type;
		this.industry_details = industry_details;
		this.address = address;
	}

	// id
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	// city
	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	// industry_type
	public void setIndustry_type(String industry_type) {
		this.industry_type = industry_type;
	}

	public String getIndustry_type() {
		return industry_type;
	}
	
	//industry_details
	public void setIndustry_details(String industry_details) {
		this.industry_details = industry_details;
	}
	public String getIndustry_details() {
		return industry_details;
	}

	// address
	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress() {
		return address;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "city: " + this.city + ", industry_type: " + this.industry_type + ",industry_details: " + this.industry_details
				+ ",address: " + this.address;
	}
}
