package com.nanjing.statistics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nanjing.statistics.model.ComprehensiveEnergyConsumption;
import com.nanjing.statistics.model.DataObject;
import com.nanjing.statistics.model.DemonstrationBase;
import com.nanjing.statistics.model.RegionIndustrialOutputValue;
import com.nanjing.statistics.model.TotalHiTechOutputValue;
import com.nanjing.statistics.result.ListResult;
import com.nanjing.statistics.result.MultiListResult;
import com.nanjing.statistics.service.IndustrialAnalysisService;

// 产业分析的接口
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/fms")
public class IndustrialAnalysisontroller {

	@Autowired
	private IndustrialAnalysisService mIAService;

	// 经济贡献率
	@RequestMapping("/contributionrate/query")
	public MultiListResult getContributionRateData() {
		System.out.println("----/contributionrate/query-----");
		MultiListResult mResult = new MultiListResult();
		List<DataObject<?>> mList = mIAService.getContributionRateData();
		System.out.println("----/contributionrate/query---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 城市工业产业总产值:11个区的工业总产值
	@RequestMapping("/riov/query")
	public ListResult<RegionIndustrialOutputValue> getRegionIndustrialOutputValueData() {
		System.out.println("----//riov/query/query-----");
		ListResult<RegionIndustrialOutputValue> mResult = new ListResult<RegionIndustrialOutputValue>();
		List<RegionIndustrialOutputValue> mList = mIAService.getRegionIndustrialOutputValueData();
		System.out.println("----/riov/query/query---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 示范基地表
	@RequestMapping("/demonbase/query")
	public ListResult<DemonstrationBase> getDemonstrationBaseData() {
		System.out.println("----/demonbase/query-----");
		ListResult<DemonstrationBase> mResult = new ListResult<DemonstrationBase>();
		List<DemonstrationBase> mList = mIAService.getDemonstrationBaseData();
		System.out.println("----/demonbase/query---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 南京市高技术产业总产值
	@RequestMapping("/hitech/query")
	public ListResult<TotalHiTechOutputValue> getHitechOutputValueData() {
		System.out.println("----/hitech/query-----");
		ListResult<TotalHiTechOutputValue> mResult = new ListResult<TotalHiTechOutputValue>();
		List<TotalHiTechOutputValue> mList = mIAService.getHitechOutputValueData();
		System.out.println("----/hitech/query---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 南京市各产业综合能耗
	@RequestMapping("/energyconsumption/query")
	public ListResult<ComprehensiveEnergyConsumption> getEnergyConsumptionData() {
		System.out.println("----/energyconsumption/query-----");
		ListResult<ComprehensiveEnergyConsumption> mResult = new ListResult<ComprehensiveEnergyConsumption>();
		List<ComprehensiveEnergyConsumption> mList = mIAService.getEnergyConsumptionData();
		System.out.println("----/energyconsumption/query---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}
}
