package com.nanjing.statistics.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.nanjing.statistics.mapper.DataMapper;
import com.nanjing.statistics.model.DataObject;
import com.nanjing.statistics.model.EcoSavingData;
import com.nanjing.statistics.model.EcoSavingRate;
import com.nanjing.statistics.model.Employee;
import com.nanjing.statistics.model.GovernmentSpecialFunds;
import com.nanjing.statistics.model.IndustrialFixedAssetInvestment;
import com.nanjing.statistics.model.IndustrialValueAdded;
import com.nanjing.statistics.model.Innovation;
import com.nanjing.statistics.model.MainBusinessIncome;
import com.nanjing.statistics.model.TotalIndustrialValue;
import com.nanjing.statistics.model.TotalProfit;
import com.nanjing.statistics.model.TotalTaxation;

@Service
@Component
public class DataService {

	@Autowired
	public DataMapper mDataMapper;

	public List<TotalIndustrialValue> getTotalIndustrialValueList(String city, int year) {
		List<TotalIndustrialValue> mList = null;
		if (city != null && !city.isEmpty()) {
			if (year > 0) {
				mList = mDataMapper.getTotalIndustrialValueListByCityAndYear(city, year);
			} else {
				mList = mDataMapper.getTotalIndustrialValueListByCity(city);
			}
		} else {
			if (year > 0) {
				mList = mDataMapper.getTotalIndustrialValueListByYear(year);
			} else {
				mList = mDataMapper.getTotalIndustrialValueList();
			}
		}
		return mList;
	}

	public List<MainBusinessIncome> getMainBusinessIncomeList() {
		List<MainBusinessIncome> mList = null;
		mList = mDataMapper.getMainBusinessIncomeList();
		return mList;
	}

	public List<IndustrialValueAdded> getIndustrialValueAddedList() {
		List<IndustrialValueAdded> mList = mDataMapper.getIVAList();
		return mList;
	}

	public List<IndustrialFixedAssetInvestment> getIndustrialFixedAssetInvestmentList() {
		List<IndustrialFixedAssetInvestment> mList = mDataMapper.getIFAIList();
		return mList;
	}

	// 总体情况
	public List<DataObject<?>> getOverallData() {
		List<DataObject<?>> mList = new ArrayList<DataObject<?>>();

		// 规模以上工业企业主营业务收入
		List<MainBusinessIncome> mBusinessIncomeList = mDataMapper.getMainBusinessIncomeList();
		if (mBusinessIncomeList != null) {
			DataObject<MainBusinessIncome> mObject = new DataObject<MainBusinessIncome>();
			mObject.setName("主营业务收入");
			mObject.setObject(mBusinessIncomeList);
			mList.add(mObject);
		}

		// 工业总产值
		List<TotalIndustrialValue> mTotalIndustrialList = mDataMapper.getTotalIndustrialValueListByCity("南京市");
		if (mTotalIndustrialList != null) {
			DataObject<TotalIndustrialValue> mObject = new DataObject<TotalIndustrialValue>();
			mObject.setName("工业总产值");
			mObject.setObject(mTotalIndustrialList);
			mList.add(mObject);
		}

		// 工业增加值
		List<IndustrialValueAdded> mIVList = mDataMapper.getIVAList();
		if (mIVList != null) {
			DataObject<IndustrialValueAdded> mObject = new DataObject<IndustrialValueAdded>();
			mObject.setName("工业增加值");
			mObject.setObject(mIVList);
			mList.add(mObject);
		}

		// 工业固定资产投资额
		List<IndustrialFixedAssetInvestment> mIFAIList = mDataMapper.getIFAIList();
		if (mIFAIList != null) {
			DataObject<IndustrialFixedAssetInvestment> mObject = new DataObject<IndustrialFixedAssetInvestment>();
			mObject.setName("工业固定资产投资额");
			mObject.setObject(mIFAIList);
			mList.add(mObject);
		}

		return mList;
	}

	public List<Innovation> getInnovationList() {
		List<Innovation> mList = mDataMapper.getInnovationList();
		return mList;
	}

	public List<GovernmentSpecialFunds> getGovernmentSpecialFundsList() {
		List<GovernmentSpecialFunds> mList = mDataMapper.getSpecialFundsList();
		return mList;
	}

	public List<DataObject<?>> getEcoSavingData() {
		List<DataObject<?>> mList = new ArrayList<DataObject<?>>();
		
		List<EcoSavingData> mEcoDataList = mDataMapper.getEcoSavingData();
		List<EcoSavingRate> mEcoRateList = mDataMapper.getEcoSavingRate();
		
		
		DataObject<EcoSavingData> mObject_Data = new DataObject<EcoSavingData>();
		mObject_Data.setName("能耗数值");
		mObject_Data.setObject(mEcoDataList);
		mList.add(mObject_Data);
		
		DataObject<EcoSavingRate> mObject_Rate = new DataObject<EcoSavingRate>();
		mObject_Rate.setName("能耗增长率");
		mObject_Rate.setObject(mEcoRateList);
		mList.add(mObject_Rate);

		return mList;
	}

	public List<DataObject<?>> getEconomicPerformanceData() {
		List<DataObject<?>> mList = new ArrayList<DataObject<?>>();

		// 利润总额
		List<TotalProfit> mProfitList = mDataMapper.getTotalProfitList();
		if (mProfitList != null) {
			DataObject<TotalProfit> mObject = new DataObject<TotalProfit>();
			mObject.setName("利润总额");
			mObject.setObject(mProfitList);
			mList.add(mObject);
		}

		// 税金总额
		List<TotalTaxation> mTaxationList = mDataMapper.getTotalTaxationList();
		if (mTaxationList != null) {
			DataObject<TotalTaxation> mObject = new DataObject<TotalTaxation>();
			mObject.setName("税金总额");
			mObject.setObject(mTaxationList);
			mList.add(mObject);
		}

		return mList;
	}

	public List<DataObject<?>> getHumanResourcesData() {
		List<DataObject<?>> mList = new ArrayList<DataObject<?>>();

		// 主营业务
		List<MainBusinessIncome> mIncomeList = mDataMapper.getMainBusinessIncomeList();
		if (mIncomeList != null) {
			DataObject<MainBusinessIncome> mObject = new DataObject<MainBusinessIncome>();
			mObject.setName("主营业务收入");
			mObject.setObject(mIncomeList);
			mList.add(mObject);
		}

		// 利润总额
		List<TotalProfit> mProfitList = mDataMapper.getTotalProfitList();
		if (mProfitList != null) {
			DataObject<TotalProfit> mObject = new DataObject<TotalProfit>();
			mObject.setName("利润总额");
			mObject.setObject(mProfitList);
			mList.add(mObject);
		}

		// 从业人员增长率
		List<Employee> mEmployeeList = mDataMapper.getEmployeeList();
		if (mEmployeeList != null) {
			DataObject<Employee> mObject = new DataObject<Employee>();
			mObject.setName("从业人员增长率");
			mObject.setObject(mEmployeeList);
			mList.add(mObject);
		}

		return mList;
	}
}
