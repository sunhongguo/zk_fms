package com.nanjing.statistics.model;

import java.io.Serializable;

//创新能力
public class Innovation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id; // ID
	private int year; // 年份
	private int patent; // 有效发明专利数
	private double input_value; // 工业企业研发机构仪器和设备投入额（单位：亿元）
	private double input_rate; // 工业企业研发机构仪器和设备投入额增速(%)

	public Innovation() {
		super();
	}

	public Innovation(int year, int patent, double input_value, double input_rate) {
		this.year = year;
		this.patent = patent;
		this.input_value = input_value;
		this.input_rate = input_rate;
	}

	// id
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	// year
	public void setYear(int year) {
		this.year = year;
	}

	public int getYear() {
		return year;
	}

	// patent
	public void setPatent(int patent) {
		this.patent = patent;
	}

	public int getPatent() {
		return patent;
	}

	// input_value
	public void setInput_value(double input_value) {
		this.input_value = input_value;
	}

	public double getInput_value() {
		return input_value;
	}

	// input_rate
	public void setInput_rate(double input_rate) {
		this.input_rate = input_rate;
	}

	public double getInput_rate() {
		return input_rate;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "year: " + this.year + ", patent: " + this.patent + ", input_value: " + this.input_value
				+ ", input_rate: " + this.input_rate;
	}

}
