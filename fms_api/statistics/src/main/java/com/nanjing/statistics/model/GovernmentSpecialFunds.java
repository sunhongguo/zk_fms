package com.nanjing.statistics.model;

import java.io.Serializable;

//政府专项支持资金
public class GovernmentSpecialFunds implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id; // ID
	private int year; // 年份
	private double value; // 政府基金预算支出（单位：亿元）
	private double rate;  // 同比增长率

	public GovernmentSpecialFunds() {
		super();
	}

	public GovernmentSpecialFunds(int year, double value, double rate) {
		this.year = year;
		this.value = value;
		this.rate = rate;
	}

	// id
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	// year
	public void setYear(int year) {
		this.year = year;
	}

	public int getYear() {
		return year;
	}

	// value
	public void setValue(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}
	
	// rate
		public void setRate(double rate) {
			this.rate = rate;
		}

		public double getRate() {
			return rate;
		}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "year: " + this.year + ", value: " + this.value + ", rate: " + this.rate;
	}
}
