package com.nanjing.statistics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nanjing.statistics.model.DataObject;
import com.nanjing.statistics.model.GovernmentSpecialFunds;
import com.nanjing.statistics.model.IndustrialFixedAssetInvestment;
import com.nanjing.statistics.model.IndustrialValueAdded;
import com.nanjing.statistics.model.Innovation;
import com.nanjing.statistics.model.MainBusinessIncome;
import com.nanjing.statistics.model.TotalIndustrialValue;
import com.nanjing.statistics.result.ListResult;
import com.nanjing.statistics.result.MBINCListResult;
import com.nanjing.statistics.result.MultiListResult;
import com.nanjing.statistics.result.TIVListResult;
import com.nanjing.statistics.service.DataService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/fms")
public class DataController {
	@Autowired
	public DataService mDataService;

	// 获取工业总产值列表
	@RequestMapping("/tiov/querylist")
	public TIVListResult getTotalIndustrialOutputValueList(String city, int year) {
		System.out.println("----/total_industrial_output_value/querylist-----");
		TIVListResult mResult = new TIVListResult();
		List<TotalIndustrialValue> mList = mDataService.getTotalIndustrialValueList(city, year);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 规模以上工业企业主营业务收入
	@RequestMapping("/mbincome/querylist")
	public MBINCListResult getMainBusinessIncomeList() {
		System.out.println("----/main_business_income/querylist-----");
		MBINCListResult mResult = new MBINCListResult();
		List<MainBusinessIncome> mList = mDataService.getMainBusinessIncomeList();
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 工业增加值
	@RequestMapping("/iva/querylist")
	public ListResult<IndustrialValueAdded> getIndustrialValueAddedList() {
		System.out.println("----/iva/querylist-----");
		ListResult<IndustrialValueAdded> mResult = new ListResult<IndustrialValueAdded>();
		List<IndustrialValueAdded> mList = mDataService.getIndustrialValueAddedList();
		System.out.println("----/iva/querylist---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 工业固定资产投资额
	@RequestMapping("/ifai/querylist")
	public ListResult<IndustrialFixedAssetInvestment> getIndustrialFixedAssetInvestmentList() {
		System.out.println("----/ifai/querylist-----");
		ListResult<IndustrialFixedAssetInvestment> mResult = new ListResult<IndustrialFixedAssetInvestment>();
		List<IndustrialFixedAssetInvestment> mList = mDataService.getIndustrialFixedAssetInvestmentList();
		System.out.println("----/ifai/querylist---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 总体情况
	@RequestMapping("/overall/query")
	public MultiListResult getOverallData() {
		System.out.println("----/overall/query-----");
		MultiListResult mResult = new MultiListResult();
		List<DataObject<?>> mList = mDataService.getOverallData();
		System.out.println("----/overall/query---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 创新能力
	@RequestMapping("/innovation/querylist")
	public ListResult<Innovation> getInnovationList() {
		System.out.println("----/innovation/querylist-----");
		ListResult<Innovation> mResult = new ListResult<Innovation>();
		List<Innovation> mList = mDataService.getInnovationList();
		System.out.println("----/innovation/querylist---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 政府专项支持资金
	@RequestMapping("/specialfunds/querylist")
	public ListResult<GovernmentSpecialFunds> getGovernmentSpecialFundsList() {
		System.out.println("----/specialfunds/querylist-----");
		ListResult<GovernmentSpecialFunds> mResult = new ListResult<GovernmentSpecialFunds>();
		List<GovernmentSpecialFunds> mList = mDataService.getGovernmentSpecialFundsList();
		System.out.println("----/specialfunds/querylist---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 环保节能数据
	@RequestMapping("/ecosaving/query")
	public MultiListResult getEcoSavingData() {
		System.out.println("----/ecosaving/query-----");
		MultiListResult mResult = new MultiListResult();
		List<DataObject<?>> mList = mDataService.getEcoSavingData();
		System.out.println("----/ecosaving/query---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 经济效益
	@RequestMapping("/economic/query")
	public MultiListResult getEconomicPerformanceData() {
		System.out.println("----/economic/query-----");
		MultiListResult mResult = new MultiListResult();
		List<DataObject<?>> mList = mDataService.getEconomicPerformanceData();
		System.out.println("----/economic/query---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 人力资源
	@RequestMapping("/hr/query")
	public MultiListResult getHumanResourcesData() {
		System.out.println("----/hr/query-----");
		MultiListResult mResult = new MultiListResult();
		List<DataObject<?>> mList = mDataService.getHumanResourcesData();
		System.out.println("----/hr/query---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

}
