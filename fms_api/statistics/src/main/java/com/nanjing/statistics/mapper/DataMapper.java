package com.nanjing.statistics.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.nanjing.statistics.model.EcoSavingData;
import com.nanjing.statistics.model.EcoSavingRate;
import com.nanjing.statistics.model.Employee;
import com.nanjing.statistics.model.GovernmentSpecialFunds;
import com.nanjing.statistics.model.IndustrialFixedAssetInvestment;
import com.nanjing.statistics.model.IndustrialValueAdded;
import com.nanjing.statistics.model.Innovation;
import com.nanjing.statistics.model.MainBusinessIncome;
import com.nanjing.statistics.model.TotalIndustrialValue;
import com.nanjing.statistics.model.TotalProfit;
import com.nanjing.statistics.model.TotalTaxation;

@Mapper
public interface DataMapper {
	@Select("SELECT * FROM db_total_industrial_output_value")
	@Results({
		@Result(property = "city", column = "city"),
		@Result(property = "year", column = "year"),
		@Result(property = "value", column = "value")
	})
	List<TotalIndustrialValue> getTotalIndustrialValueList();
	
	@Select("SELECT * FROM db_total_industrial_output_value WHERE city = #{city}")
	@Results({
		@Result(property = "city", column = "city"),
		@Result(property = "year", column = "year"),
		@Result(property = "value", column = "value")
	})
	List<TotalIndustrialValue> getTotalIndustrialValueListByCity(String city);
	
	@Select("SELECT * FROM db_total_industrial_output_value WHERE year = #{year}")
	List<TotalIndustrialValue> getTotalIndustrialValueListByYear(int year);
	
	@Select("SELECT * FROM db_total_industrial_output_value WHERE city = #{city} AND year = #{year}")
	List<TotalIndustrialValue> getTotalIndustrialValueListByCityAndYear(@Param("city")String city, @Param("year")int year);
	
	
	@Select("SELECT * FROM db_main_business_income_nj")
	@Results({
		@Result(property = "year", column = "year"),
		@Result(property = "totalize", column = "totalize"),
		@Result(property = "income", column = "income")
	})
	List<MainBusinessIncome> getMainBusinessIncomeList();
	
	
	@Select("SELECT * FROM db_industrial_value_added_nj")
	List<IndustrialValueAdded> getIVAList();

	@Select("SELECT * FROM db_industrial_fixed_asset_investment_nj")
	List<IndustrialFixedAssetInvestment> getIFAIList();
	
	@Select("SELECT * FROM db_innovation_nj")
	List<Innovation> getInnovationList();

	// 利润总额
	@Select("SELECT * FROM db_total_profit_nj")
	List<TotalProfit> getTotalProfitList();
	
	// 税金总额
	@Select("SELECT * FROM db_total_taxation_nj")
	List<TotalTaxation> getTotalTaxationList();

	// 政府专项支持
	@Select("SELECT * FROM db_government_special_funds_nj ORDER BY year DESC")
	List<GovernmentSpecialFunds> getSpecialFundsList();
	
	// 查询节能环保数据
	@Select("SELECT * FROM db_ecosaving_data_nj ORDER BY year DESC limit 2")
	List<EcoSavingData> getEcoSavingData();
	
	// 查询节能环保数据增长率
	@Select("SELECT * FROM db_ecosaving_data_nj ORDER BY year DESC limit 1")
	List<EcoSavingRate> getEcoSavingRate();
	
	// 从业人员
	@Select("SELECT * FROM db_employee_nj")
	List<Employee> getEmployeeList();
}
