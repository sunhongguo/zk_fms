package com.nanjing.statistics.result;

import java.util.List;

public class ListResult<T> {
	private int code;
	private String message;
	private List<T> object;
	
	public ListResult(){
		super();
	}
	
	public void setCode(int i){
		this.code = i;
	}
	public int getCode(){
		return code;
	}
	
	public void setMessage(String msg){
		this.message = msg;
	}
	public String getMessage(){
		return message;
	}
	
	public void setObject(List<T> object){
		this.object = object;
	}
	public List<T> getObject(){
		return object;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "code: " + this.code + ", message: " + this.message + ", object: " + object;
	}

}
