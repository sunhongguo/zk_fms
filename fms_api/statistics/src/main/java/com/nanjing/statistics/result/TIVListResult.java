package com.nanjing.statistics.result;

import java.io.Serializable;
import java.util.List;

import com.nanjing.statistics.model.TotalIndustrialValue;

public class TIVListResult implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int code;
	private String message;
	private List<TotalIndustrialValue> object;
	
	public TIVListResult(){
		super();
	}
	
	public void setCode(int i){
		this.code = i;
	}
	public int getCode(){
		return code;
	}
	
	public void setMessage(String msg){
		this.message = msg;
	}
	public String getMessage(){
		return message;
	}
	
	public void setObject(List<TotalIndustrialValue> object){
		this.object = object;
	}
	public List<TotalIndustrialValue> getObject(){
		return object;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "code: " + this.code + ", message: " + this.message + ", object: " + object;
	}

}
