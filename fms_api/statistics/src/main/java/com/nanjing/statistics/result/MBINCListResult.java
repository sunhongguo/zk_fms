package com.nanjing.statistics.result;

import java.io.Serializable;
import java.util.List;

import com.nanjing.statistics.model.MainBusinessIncome;

//规模以上工业企业主营业务收入
public class MBINCListResult implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int code;
	private String message;
	private List<MainBusinessIncome> object;
	
	public MBINCListResult(){
		super();
	}
	
	public void setCode(int i){
		this.code = i;
	}
	public int getCode(){
		return code;
	}
	
	public void setMessage(String msg){
		this.message = msg;
	}
	public String getMessage(){
		return message;
	}
	
	public void setObject(List<MainBusinessIncome> object){
		this.object = object;
	}
	public List<MainBusinessIncome> getObject(){
		return object;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "code: " + this.code + ", message: " + this.message + ", object: " + object;
	}

}
