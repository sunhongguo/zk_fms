package com.nanjing.statistics.model;

import java.util.List;

// 数据信息
public class DataObject<T> {
	private String name;  // 数据的名称
	private List<T> object; 
	
	public DataObject(String name, List<T> object){
		this.name = name;
		this.object = object;
	}
	
	public DataObject() {
		// TODO Auto-generated constructor stub
		super();
	}

	// name
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	
	// object
	public void setObject(List<T> object){
		this.object = object;
	}
	public List<T> getObject(){
		return object;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "name: " + this.name + ", object: " + object ;
	}
}
