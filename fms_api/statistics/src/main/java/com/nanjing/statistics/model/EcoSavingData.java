package com.nanjing.statistics.model;

import java.io.Serializable;

public class EcoSavingData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int year; // 年份
	private double coal_value; // 耗煤量
	private double electricity_value; // 用电量
	private double water_value; // 用水量
	
	public EcoSavingData(){
		super();
	}
	
	public EcoSavingData(int year, double coal_value, double electricity_value, double water_value){
		this.year = year;
		this.coal_value = coal_value;
		this.electricity_value = electricity_value;
		this.water_value = water_value;
	}
	
	public void setYear(int year){
		this.year = year;
	}
	public int getYear(){
		return year;
	}
	
	public void setCoal_value(double coal_value){
		this.coal_value = coal_value;
	}
	public double getCoal_value(){
		return coal_value;
	}
	
	public void setElectricity_value(double electricity_value){
		this.electricity_value = electricity_value;
	}
	public double getElectricity_value(){
		return electricity_value;
	}
	
	public void setWater_value(double water_value){
		this.water_value = water_value;
	}
	public double getWaterValue(){
		return water_value;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "year: " + this.year + ",coal_value: " + this.coal_value + ",electricity_value: " + this.electricity_value
				+ ",water_value: " + this.water_value;
	}
}
