package com.nanjing.statistics.model;

import java.io.Serializable;

// 第二产业从业人员（万人）
public class Employee implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id; // ID
	private int year; // 年份
	private double number; // 从业人员（万人）
	private double rate; // 增长率%

	public Employee() {
		super();
	}

	public Employee(int year, double number, double rate) {
		this.year = year;
		this.number = number;
		this.rate = rate;
	}

	// id
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	// year
	public void setYear(int year) {
		this.year = year;
	}

	public int getYear() {
		return year;
	}

	// number
	public void setNumber(double number) {
		this.number = number;
	}

	public double getNumber() {
		return number;
	}
	
	// rate
		public void setRate(double rate) {
			this.rate = rate;
		}

		public double getRate() {
			return rate;
		}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "year: " + this.year + ",number: " + this.number + ",rate: " + this.rate;
	}

}
