package com.nanjing.statistics.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nanjing.statistics.mapper.IndustrialAnalysisMapper;
import com.nanjing.statistics.model.ComprehensiveEnergyConsumption;
import com.nanjing.statistics.model.ContributionRate;
import com.nanjing.statistics.model.DataObject;
import com.nanjing.statistics.model.DemonstrationBase;
import com.nanjing.statistics.model.RegionIndustrialOutputValue;
import com.nanjing.statistics.model.TotalHiTechOutputValue;

// 产业分析
@Service
public class IndustrialAnalysisService {

	@Autowired
	private IndustrialAnalysisMapper mIAMapper;

	// 经济贡献率
	public List<DataObject<?>> getContributionRateData() {
		List<DataObject<?>> mList = new ArrayList<DataObject<?>>();
		List<Integer> mYearList = mIAMapper.getContributionRateYear();
		if(mYearList != null){
			for(int year:mYearList){
				List<ContributionRate> mCList = mIAMapper.getContributionRateListByYear(year);
				if (mCList != null) {
					DataObject<ContributionRate> mObject = new DataObject<ContributionRate>();
					mObject.setName(year + "");
					mObject.setObject(mCList);
					mList.add(mObject);
				}
			}
		}
//		List<ContributionRate> mList = mIAMapper.getContributionRateList();
		return mList;
	}

	// 城市工业产业总产值
	public List<RegionIndustrialOutputValue> getRegionIndustrialOutputValueData() {
		List<RegionIndustrialOutputValue> mList = mIAMapper.getRegionIndustrialOutputValueList();
		return mList;
	}

	// 示范基地表
	public List<DemonstrationBase> getDemonstrationBaseData() {
		List<DemonstrationBase> mList = mIAMapper.getDemonstrationBaseList();
		return mList;
	}

	// 南京市高技术产业总产值
	public List<TotalHiTechOutputValue> getHitechOutputValueData() {
		List<TotalHiTechOutputValue> mList = mIAMapper.getHiTechOutputValueList();
		return mList;
	}

	// 南京市各产业综合能耗
	public List<ComprehensiveEnergyConsumption> getEnergyConsumptionData() {
		List<ComprehensiveEnergyConsumption> mList = mIAMapper.getComEnergyConsumptionList();
		return mList;
	}
}
