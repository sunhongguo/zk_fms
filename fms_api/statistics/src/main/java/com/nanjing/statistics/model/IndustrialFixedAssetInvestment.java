package com.nanjing.statistics.model;

import java.io.Serializable;

//工业固定资产投资额
public class IndustrialFixedAssetInvestment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id; // ID
	private int year; // 年份
	private double value; // 增加值

	public IndustrialFixedAssetInvestment() {
		super();
	}

	public IndustrialFixedAssetInvestment(int year, double value) {
		this.year = year;
		this.value = value;
	}

	// id
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	// year
	public void setYear(int year) {
		this.year = year;
	}

	public int getYear() {
		return year;
	}

	// value
	public void setValue(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "year: " + this.year + ", value: " + this.value;
	}
}

