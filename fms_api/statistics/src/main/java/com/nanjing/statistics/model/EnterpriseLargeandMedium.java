package com.nanjing.statistics.model;

import java.io.Serializable;

// 私营企业
public class EnterpriseLargeandMedium implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id; // ID
	private int year; // 年份
	private int month; // 月份
	private int day; // 天
	private double totalize; // 累计值
	private double rate; // 增长率%
	private int number;  // 企业数量

	public EnterpriseLargeandMedium() {
		super();
	}

	public EnterpriseLargeandMedium(int year, int month, int day, double totalize, double rate,int number) {
		this.year = year;
		this.month = month;
		this.day = day;
		this.totalize = totalize;
		this.rate = rate;
		this.number = number;
	}

	// id
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	// year
	public void setYear(int year) {
		this.year = year;
	}

	public int getYear() {
		return year;
	}

	// month
	public void setMonth(int month) {
		this.month = month;
	}

	public int getMonth() {
		return month;
	}

	// day
	public void setDay(int day) {
		this.day = day;
	}

	public int getDay() {
		return day;
	}

	// totalize
	public void setTotalize(double totalize) {
		this.totalize = totalize;
	}

	public double getTotalize() {
		return totalize;
	}

	// rate
	public void setRate(double rate) {
		this.rate = rate;
	}

	public double getRate() {
		return rate;
	}

	// number
	public void setNumber(int number){
		this.number = number;
	}
	public int getNumber(){
		return number;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "year: " + this.year + ", totalize: " + this.totalize + ", rate: " + this.rate
				+ ", number: " + this.number;
	}

}
