package com.nanjing.statistics.model;

import java.io.Serializable;

// 工业总产值
public class TotalIndustrialValue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id; // ID
	private String city; // 城市
	private int year; // 年份
	private double value; // 总数值

	public TotalIndustrialValue() {
		super();
	}

	public TotalIndustrialValue(String city, int year, double value) {
		this.city = city;
		this.year = year;
		this.value = value;
	}

	// id
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	// city
	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	// year
	public void setYear(int year) {
		this.year = year;
	}

	public int getYear() {
		return year;
	}

	// value
	public void setValue(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "city: " + this.city + ", year: "+ this.year + ",value: " + this.value;
	}
}
