package com.nanjing.statistics.model;

public class EcoSavingRate {
	private int year; // 年份
	private double coal_rate; // 耗煤量同比增长率%
	private double electricity_rate; // 用电量同比增长率%
	private double water_rate; // 用水量同比增长率%
	
	public EcoSavingRate(){
		super();
	}
	
	public EcoSavingRate(int year, double coal_rate, double electricity_rate, double water_rate){
		this.year = year;
		this.coal_rate = coal_rate;
		this.electricity_rate = electricity_rate;
		this.water_rate = water_rate;
	}
	
	public void setYear(int year){
		this.year = year;
	}
	public int getYear(){
		return year;
	}
	
	public void setCoal_rate(double coal_rate){
		this.coal_rate = coal_rate;
	}
	public double getCoal_rate(){
		return coal_rate;
	}
	
	public void setElectricity_rate(double electricity_rate){
		this.electricity_rate = electricity_rate;
	}
	public double getElectricity_rate(){
		return electricity_rate;
	}
	
	public void setWater_rate(double water_rate){
		this.water_rate = water_rate;
	}
	public double getWater_rate(){
		return water_rate;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "year: " + this.year + ",coal_rate: " + this.coal_rate + ",electricity_rate: " + this.electricity_rate
				+ ",water_rate: " + this.water_rate;
	}
}
