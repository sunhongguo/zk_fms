package com.nanjing.statistics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nanjing.statistics.model.EnterpriseLargeandMedium;
import com.nanjing.statistics.model.EnterprisePrivate;
import com.nanjing.statistics.model.EnterpriseState;
import com.nanjing.statistics.model.RegionOutputValue;
import com.nanjing.statistics.result.ListResult;
import com.nanjing.statistics.service.RegionAnalysisService;

//区域分析的接口
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/fms")
public class RegionAnalysisontroller {

	@Autowired
	private RegionAnalysisService mRAService;

	// 区域总产值:销售收入
	@RequestMapping("/regionoutput/query")
	public ListResult<RegionOutputValue> getRegionOutputValueData(int year) {
		System.out.println("----/regionoutput/query-----");
		ListResult<RegionOutputValue> mResult = new ListResult<RegionOutputValue>();
		List<RegionOutputValue> mList = mRAService.getRegionOutputValueData(year);
		System.out.println("----/regionoutput/query---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}
	
	
	// 大中型企业
	@RequestMapping("/enterprise_lm/query")
	public ListResult<EnterpriseLargeandMedium> getEnterpriseLargeandMediumData() {
		System.out.println("----/enterprise_lm/query-----");
		ListResult<EnterpriseLargeandMedium> mResult = new ListResult<EnterpriseLargeandMedium>();
		List<EnterpriseLargeandMedium> mList = mRAService.getEnterpriseLMData();
		System.out.println("----/enterprise_lm/query---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 国有及国有控股企业
	@RequestMapping("/enterprise_state/query")
	public ListResult<EnterpriseState> getEnterpriseStateData() {
		System.out.println("----/enterprise_state/query-----");
		ListResult<EnterpriseState> mResult = new ListResult<EnterpriseState>();
		List<EnterpriseState> mList = mRAService.getEnterpriseStateData();
		System.out.println("----/enterprise_state/query---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}

	// 私营企业
	@RequestMapping("/enterprise_private/query")
	public ListResult<EnterprisePrivate> getEnterprisePrivateData() {
		System.out.println("----/enterprise_private/query-----");
		ListResult<EnterprisePrivate> mResult = new ListResult<EnterprisePrivate>();
		List<EnterprisePrivate> mList = mRAService.getEnterprisePrivateData();
		System.out.println("----/enterprise_private/query---result--" + mList);
		if (mList != null) {
			mResult.setCode(0);
			mResult.setMessage("获取成功！");
			mResult.setObject(mList);
		} else {
			mResult.setCode(-1);
			mResult.setMessage("获取失败！");
		}
		return mResult;
	}
	

}
