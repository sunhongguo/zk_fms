package com.nanjing.statistics.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nanjing.statistics.mapper.RegionAnalysisMapper;
import com.nanjing.statistics.model.EnterpriseLargeandMedium;
import com.nanjing.statistics.model.EnterprisePrivate;
import com.nanjing.statistics.model.EnterpriseState;
import com.nanjing.statistics.model.RegionOutputValue;

// 区域分析
@Service
public class RegionAnalysisService {

	@Autowired
	public RegionAnalysisMapper mRAMapper;

	// 区域总产值
	public List<RegionOutputValue> getRegionOutputValueData(int year) {
		List<RegionOutputValue> mList = null;
		if (year > 0) {
			mList = mRAMapper.getRegionOutputValueListByYear(year);
		} else {
			mList = mRAMapper.getRegionOutputValueList();
		}
		return mList;
	}

	public List<EnterpriseLargeandMedium> getEnterpriseLMData() {
		List<EnterpriseLargeandMedium> mList = mRAMapper.getEnterpriseLM();
		return mList;
	}

	public List<EnterpriseState> getEnterpriseStateData() {
		List<EnterpriseState> mList = mRAMapper.getEnterpriseState();
		return mList;
	}

	public List<EnterprisePrivate> getEnterprisePrivateData() {
		List<EnterprisePrivate> mList = mRAMapper.getEnterprisePrivate();
		return mList;
	}
}
