package com.nanjing.statistics.result;

import java.util.List;

import com.nanjing.statistics.model.DataObject;

// 包含多个列表的返回值
public class MultiListResult {
	private int code;
	private String message;
	private List<DataObject<?>> object;
	
	public MultiListResult(){
		super();
	}
	
	public void setCode(int i){
		this.code = i;
	}
	public int getCode(){
		return code;
	}
	
	public void setMessage(String msg){
		this.message = msg;
	}
	public String getMessage(){
		return message;
	}
	
	public void setObject(List<DataObject<?>> object){
		this.object = object;
	}
	public List<DataObject<?>> getObject(){
		return object;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "code: " + this.code + ", message: " + this.message + ", object: " + object;
	}

}