-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.7.21-log - MySQL Community Server (GPL)
-- 服务器OS:                        Win64
-- HeidiSQL 版本:                  10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for nanjing_data
DROP DATABASE IF EXISTS `nanjing_data`;
CREATE DATABASE IF NOT EXISTS `nanjing_data` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `nanjing_data`;

-- Dumping structure for table nanjing_data.db_comprehensive_energy_consumption_nj
DROP TABLE IF EXISTS `db_comprehensive_energy_consumption_nj`;
CREATE TABLE IF NOT EXISTS `db_comprehensive_energy_consumption_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年',
  `month` int(11) NOT NULL DEFAULT '0' COMMENT '月',
  `day` int(11) NOT NULL DEFAULT '0' COMMENT '日',
  `totalize` double NOT NULL DEFAULT '0' COMMENT '规模以上工业:综合能耗:累计值(单位：亿元)',
  `value` double NOT NULL DEFAULT '0' COMMENT '规模以上工业:综合能耗(单位：亿元)',
  `rate` double NOT NULL DEFAULT '0' COMMENT '累计同比%',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='南京市各产业综合能耗（单位：亿元）';

-- Dumping data for table nanjing_data.db_comprehensive_energy_consumption_nj: ~4 rows (大约)
/*!40000 ALTER TABLE `db_comprehensive_energy_consumption_nj` DISABLE KEYS */;
INSERT INTO `db_comprehensive_energy_consumption_nj` (`id`, `year`, `month`, `day`, `totalize`, `value`, `rate`) VALUES
	(1, 2015, 12, 31, 3670.95, 3670.95, 0.9),
	(2, 2016, 11, 30, 3505.67, 3824.37, 5.1),
	(3, 2017, 12, 31, 3784.38, 3784.38, -1.2),
	(4, 2018, 12, 31, 3826.97, 3826.97, 0.7);
/*!40000 ALTER TABLE `db_comprehensive_energy_consumption_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_contribution_rate
DROP TABLE IF EXISTS `db_contribution_rate`;
CREATE TABLE IF NOT EXISTS `db_contribution_rate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `city` varchar(50) NOT NULL DEFAULT '' COMMENT '城市',
  `value` double NOT NULL DEFAULT '0' COMMENT '贡献率',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='各产业全国经济贡献率';

-- Dumping data for table nanjing_data.db_contribution_rate: ~26 rows (大约)
/*!40000 ALTER TABLE `db_contribution_rate` DISABLE KEYS */;
INSERT INTO `db_contribution_rate` (`id`, `year`, `city`, `value`) VALUES
	(2, 2016, '南京', 39.2),
	(3, 2017, '南京', 38.03),
	(5, 2016, '南通', 46.8),
	(6, 2017, '南通', 47.06),
	(8, 2016, '宿迁', 48.5),
	(9, 2017, '宿迁', 48.01),
	(11, 2016, '常州', 46.46),
	(12, 2017, '常州', 46.82),
	(14, 2016, '徐州', 43.3),
	(15, 2017, '徐州', 43.66),
	(17, 2016, '扬州', 49.4),
	(18, 2017, '扬州', 48.88),
	(20, 2016, '无锡', 47.2),
	(21, 2017, '无锡', 47.23),
	(23, 2016, '泰州', 47.1),
	(24, 2017, '泰州', 47.17),
	(26, 2016, '淮安', 41.6),
	(27, 2017, '淮安', 42.25),
	(29, 2016, '盐城', 44.8),
	(30, 2017, '盐城', 44.4),
	(32, 2016, '苏州', 47),
	(33, 2017, '苏州', 47.55),
	(35, 2016, '连云港', 44.2),
	(36, 2017, '连云港', 44.69),
	(38, 2016, '镇江', 48.8),
	(39, 2017, '镇江', 49.32);
/*!40000 ALTER TABLE `db_contribution_rate` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_demonstration_base
DROP TABLE IF EXISTS `db_demonstration_base`;
CREATE TABLE IF NOT EXISTS `db_demonstration_base` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `city` varchar(50) NOT NULL DEFAULT '' COMMENT '所在城市',
  `industry_type` varchar(256) NOT NULL DEFAULT '' COMMENT '行业类型',
  `industry_details` varchar(256) NOT NULL DEFAULT '' COMMENT '行业类型细分',
  `address` varchar(512) NOT NULL DEFAULT '' COMMENT '基地所在位置，具体地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='示范基地表';

-- Dumping data for table nanjing_data.db_demonstration_base: ~24 rows (大约)
/*!40000 ALTER TABLE `db_demonstration_base` DISABLE KEYS */;
INSERT INTO `db_demonstration_base` (`id`, `city`, `industry_type`, `industry_details`, `address`) VALUES
	(1, '无锡', '电子信息产业', '电子信息（传感网）', '江苏无锡高新技术产业开发区'),
	(2, '苏州', '电子信息产业', '电子信息', '江苏苏州工业园区'),
	(3, '苏州', '电子信息产业', '电子信息（光电显示）', '江苏昆山经济技术开发区'),
	(4, '徐州', '装备制造产业', '装备制造（工程机械）', '江苏省徐州市'),
	(5, '南通', '海洋工程与船舶产业', '海洋工程与船舶产业', '南通市'),
	(6, '无锡', '环保装备产业', '环保装备产业', '江苏宜兴'),
	(7, '南京', '电子信息产业', '电子信息产业', '南京江宁经济技术开发区'),
	(8, '南京', '软件和信息服务产业', '软件和信息服务', '南京雨花软件园'),
	(9, '无锡', '装备制造产业', '装备制造', '江苏江阴临港经济开发区'),
	(10, '苏州', '电子信息产业', '电子信息（光电子）', '江苏吴江经济技术开发区'),
	(11, '泰州', '医药产业', '医药', '江苏泰州医药高新技术产业开发区'),
	(12, '南京', '装备制造产业', '装备制造（智能电网装备）', '南京市江宁区'),
	(13, '常州', '装备制造产业', '装备制造（轨道交通装备）', '江苏常州戚墅堰'),
	(14, '苏州', '电子信息产业', '电子信息', '苏州高新技术产业开发区'),
	(15, '镇江', '军民结合产业', '军民结合', '江苏丹阳'),
	(16, '南通', '纺织产业', '纺织（家用纺织品）', '江苏南通'),
	(17, '苏州', '装备制造产业', '装备制造', '江苏张家港经济技术开发区'),
	(18, '盐城', '环保装备产业', '环保装备', '江苏盐城环保产业园'),
	(19, '常州', '电子信息产业', '电子信息（新型电子元器件）', '江苏武进高新技术产业开发区'),
	(20, '镇江', '航空产业', '航空产业（零部件）', '江苏镇江'),
	(21, '苏州', '汽车产业', '汽车产业（关键核心零部件）', '江苏太仓'),
	(22, '连云港', '医药产业', '医药', '江苏连云港经济技术开发区'),
	(23, '泰州', '新材料产业', '新材料（化工）', '江苏泰兴经济开发区'),
	(24, '南通', '大型数据中心', '大型数据中心（实时应用类）', '江苏南通国际数据中心产业园');
/*!40000 ALTER TABLE `db_demonstration_base` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_ecosaving_data_nj
DROP TABLE IF EXISTS `db_ecosaving_data_nj`;
CREATE TABLE IF NOT EXISTS `db_ecosaving_data_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `coal_value` double NOT NULL DEFAULT '0' COMMENT '单位工业增加值能耗（吨标煤/万元）',
  `coal_rate` double NOT NULL DEFAULT '0' COMMENT '单位工业增加值能耗增长率%',
  `electricity_value` double NOT NULL DEFAULT '0' COMMENT '全社会用电量（单位：亿千瓦时）',
  `electricity_rate` double NOT NULL DEFAULT '0' COMMENT '全社会用电量增长率%',
  `water_value` double NOT NULL DEFAULT '0' COMMENT '单位工业增加值用水量（立方米/万元）',
  `water_rate` double NOT NULL DEFAULT '0' COMMENT '单位工业增加值用水量增长率%',
  `water_repeat` double NOT NULL DEFAULT '0' COMMENT '重复用水总量（立方米）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='南京市节能环保';

-- Dumping data for table nanjing_data.db_ecosaving_data_nj: ~4 rows (大约)
/*!40000 ALTER TABLE `db_ecosaving_data_nj` DISABLE KEYS */;
INSERT INTO `db_ecosaving_data_nj` (`id`, `year`, `coal_value`, `coal_rate`, `electricity_value`, `electricity_rate`, `water_value`, `water_rate`, `water_repeat`) VALUES
	(1, 2015, 3670.95, 0.87, 0, 0, 15.22, 0, 69.96),
	(2, 2016, 3822.21, 3.95, 305.38, 0, 15.48, 1.68, 72.23),
	(3, 2017, 3784.38, -1.2, 318.14, 4.01, 15.75, 1.71, 73.27);
/*!40000 ALTER TABLE `db_ecosaving_data_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_employee_nj
DROP TABLE IF EXISTS `db_employee_nj`;
CREATE TABLE IF NOT EXISTS `db_employee_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `number` double NOT NULL DEFAULT '0' COMMENT '从业人员（万人）',
  `rate` double NOT NULL DEFAULT '0' COMMENT '增长率%',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='第二产业从业人员（万人）';

-- Dumping data for table nanjing_data.db_employee_nj: ~3 rows (大约)
/*!40000 ALTER TABLE `db_employee_nj` DISABLE KEYS */;
INSERT INTO `db_employee_nj` (`id`, `year`, `number`, `rate`) VALUES
	(2, 2015, 98.82, -0.138898571),
	(3, 2016, 90.68, -0.082371989),
	(4, 2017, 90.62, -0.000661667);
/*!40000 ALTER TABLE `db_employee_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_enterprise_largeandmedium_nj
DROP TABLE IF EXISTS `db_enterprise_largeandmedium_nj`;
CREATE TABLE IF NOT EXISTS `db_enterprise_largeandmedium_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `month` int(11) NOT NULL DEFAULT '0' COMMENT '月份',
  `day` int(11) NOT NULL DEFAULT '0' COMMENT '天',
  `totalize` double NOT NULL DEFAULT '0' COMMENT '累计值 单位：亿元',
  `rate` double NOT NULL DEFAULT '0' COMMENT '累计同比增长%',
  `number` int(11) NOT NULL DEFAULT '0' COMMENT '企业数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='大中型企业';

-- Dumping data for table nanjing_data.db_enterprise_largeandmedium_nj: ~3 rows (大约)
/*!40000 ALTER TABLE `db_enterprise_largeandmedium_nj` DISABLE KEYS */;
INSERT INTO `db_enterprise_largeandmedium_nj` (`id`, `year`, `month`, `day`, `totalize`, `rate`, `number`) VALUES
	(1, 2017, 12, 31, 8575.48, -0.03, 255),
	(2, 2016, 12, 31, 8875.42, -0.01, 512),
	(3, 2015, 12, 31, 8980.37, -3.5, 0);
/*!40000 ALTER TABLE `db_enterprise_largeandmedium_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_enterprise_private_nj
DROP TABLE IF EXISTS `db_enterprise_private_nj`;
CREATE TABLE IF NOT EXISTS `db_enterprise_private_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `month` int(11) NOT NULL DEFAULT '0' COMMENT '月份',
  `day` int(11) NOT NULL DEFAULT '0' COMMENT '日期',
  `totalize` double NOT NULL DEFAULT '0' COMMENT '累计值',
  `rate` double NOT NULL DEFAULT '0' COMMENT '累计同比增长率%',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='私营企业';

-- Dumping data for table nanjing_data.db_enterprise_private_nj: ~3 rows (大约)
/*!40000 ALTER TABLE `db_enterprise_private_nj` DISABLE KEYS */;
INSERT INTO `db_enterprise_private_nj` (`id`, `year`, `month`, `day`, `totalize`, `rate`) VALUES
	(1, 2017, 12, 31, 2106.82, -0.12),
	(2, 2016, 12, 31, 2392.13, 0.01),
	(3, 2015, 12, 31, 2360.87, 3.7);
/*!40000 ALTER TABLE `db_enterprise_private_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_enterprise_state_nj
DROP TABLE IF EXISTS `db_enterprise_state_nj`;
CREATE TABLE IF NOT EXISTS `db_enterprise_state_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `month` int(11) NOT NULL DEFAULT '0' COMMENT '月份',
  `day` int(11) NOT NULL DEFAULT '0' COMMENT '天',
  `totalize` double NOT NULL DEFAULT '0' COMMENT '累计值',
  `rate` double NOT NULL DEFAULT '0' COMMENT '累计同步增长%',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='国有及国有控股企业';

-- Dumping data for table nanjing_data.db_enterprise_state_nj: ~3 rows (大约)
/*!40000 ALTER TABLE `db_enterprise_state_nj` DISABLE KEYS */;
INSERT INTO `db_enterprise_state_nj` (`id`, `year`, `month`, `day`, `totalize`, `rate`) VALUES
	(1, 2017, 12, 31, 4989.22, 0.23),
	(2, 2016, 12, 31, 4053.55, 0.01),
	(3, 2015, 12, 31, 3993.79, -9.9);
/*!40000 ALTER TABLE `db_enterprise_state_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_government_special_funds_nj
DROP TABLE IF EXISTS `db_government_special_funds_nj`;
CREATE TABLE IF NOT EXISTS `db_government_special_funds_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `value` double NOT NULL DEFAULT '0' COMMENT '政府基金预算支出（单位：亿元）',
  `rate` double NOT NULL DEFAULT '0' COMMENT '同比增长率',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='南京市政府专项支持资金( 单位：亿元）';

-- Dumping data for table nanjing_data.db_government_special_funds_nj: ~4 rows (大约)
/*!40000 ALTER TABLE `db_government_special_funds_nj` DISABLE KEYS */;
INSERT INTO `db_government_special_funds_nj` (`id`, `year`, `value`, `rate`) VALUES
	(1, 2015, 880.7, -5.08),
	(2, 2016, 1479.3, 67.97),
	(3, 2017, 1612, 8.97),
	(4, 2018, 1589.9, -1.37);
/*!40000 ALTER TABLE `db_government_special_funds_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_industrial_fixed_asset_investment_nj
DROP TABLE IF EXISTS `db_industrial_fixed_asset_investment_nj`;
CREATE TABLE IF NOT EXISTS `db_industrial_fixed_asset_investment_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0',
  `value` double NOT NULL DEFAULT '0' COMMENT '投资额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='南京市工业固定资产投资额（单位：亿元）';

-- Dumping data for table nanjing_data.db_industrial_fixed_asset_investment_nj: ~3 rows (大约)
/*!40000 ALTER TABLE `db_industrial_fixed_asset_investment_nj` DISABLE KEYS */;
INSERT INTO `db_industrial_fixed_asset_investment_nj` (`id`, `year`, `value`) VALUES
	(1, 2015, 2283.31),
	(2, 2016, 1784.22),
	(3, 2017, 2093.03);
/*!40000 ALTER TABLE `db_industrial_fixed_asset_investment_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_industrial_value_added_nj
DROP TABLE IF EXISTS `db_industrial_value_added_nj`;
CREATE TABLE IF NOT EXISTS `db_industrial_value_added_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `value` double NOT NULL DEFAULT '0' COMMENT '工业增加值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='南京市工业增加值（单位：亿元）';

-- Dumping data for table nanjing_data.db_industrial_value_added_nj: ~3 rows (大约)
/*!40000 ALTER TABLE `db_industrial_value_added_nj` DISABLE KEYS */;
INSERT INTO `db_industrial_value_added_nj` (`id`, `year`, `value`) VALUES
	(1, 2015, 3043.5),
	(2, 2016, 3050.55),
	(3, 2017, 3363.37);
/*!40000 ALTER TABLE `db_industrial_value_added_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_innovation_nj
DROP TABLE IF EXISTS `db_innovation_nj`;
CREATE TABLE IF NOT EXISTS `db_innovation_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `patent` int(11) NOT NULL DEFAULT '0' COMMENT '有效发明专利数',
  `input_value` double NOT NULL DEFAULT '0' COMMENT '工业企业研发机构仪器和设备投入额（单位：亿元）',
  `input_rate` double NOT NULL DEFAULT '0' COMMENT '工业企业研发机构仪器和设备投入额增速(%)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='南京市创新能力表：有关创新能力的不同数值';

-- Dumping data for table nanjing_data.db_innovation_nj: ~1 rows (大约)
/*!40000 ALTER TABLE `db_innovation_nj` DISABLE KEYS */;
INSERT INTO `db_innovation_nj` (`id`, `year`, `patent`, `input_value`, `input_rate`) VALUES
	(1, 2018, 44089, 162.62, 6.8);
/*!40000 ALTER TABLE `db_innovation_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_main_business_income_nj
DROP TABLE IF EXISTS `db_main_business_income_nj`;
CREATE TABLE IF NOT EXISTS `db_main_business_income_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'key id',
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `totalize` double NOT NULL DEFAULT '0' COMMENT '累计值',
  `average` double NOT NULL DEFAULT '0' COMMENT '平均值',
  `income` double NOT NULL DEFAULT '0' COMMENT '主营业务收入',
  `rate` double NOT NULL DEFAULT '0' COMMENT '同比增长率',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='规模以上工业企业主营业务收入,南京地区数据（单位：亿元）';

-- Dumping data for table nanjing_data.db_main_business_income_nj: ~3 rows (大约)
/*!40000 ALTER TABLE `db_main_business_income_nj` DISABLE KEYS */;
INSERT INTO `db_main_business_income_nj` (`id`, `year`, `totalize`, `average`, `income`, `rate`) VALUES
	(1, 2015, 11022.76, 918.56, 11022.76, -5.3),
	(2, 2016, 12421.68, 1035.14, 12421.68, 2.2),
	(3, 2017, 10777.32, 979.76, 11757.08, -5.35);
/*!40000 ALTER TABLE `db_main_business_income_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_region_industrial_output_value_nj
DROP TABLE IF EXISTS `db_region_industrial_output_value_nj`;
CREATE TABLE IF NOT EXISTS `db_region_industrial_output_value_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `region` varchar(50) NOT NULL DEFAULT '0' COMMENT '区域',
  `value` double NOT NULL DEFAULT '0' COMMENT '总产值（单位：亿元）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='城市工业产业总产值：11个区的工业总产值排名';

-- Dumping data for table nanjing_data.db_region_industrial_output_value_nj: ~10 rows (大约)
/*!40000 ALTER TABLE `db_region_industrial_output_value_nj` DISABLE KEYS */;
INSERT INTO `db_region_industrial_output_value_nj` (`id`, `year`, `region`, `value`) VALUES
	(1, 2017, '江宁区', 3252.28),
	(2, 2017, '鼓楼区', 150.22),
	(3, 2017, '栖霞区', 2718.91),
	(4, 2017, '秦淮区', 191.91),
	(5, 2017, '玄武区', 42.28),
	(6, 2017, '溧水区', 1091.25),
	(7, 2017, '高淳区', 944.43),
	(8, 2017, '建邺区', 4.25),
	(9, 2017, '六合区', 1681.11),
	(10, 2017, '浦口区', 1569.52);
/*!40000 ALTER TABLE `db_region_industrial_output_value_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_region_output_value_nj
DROP TABLE IF EXISTS `db_region_output_value_nj`;
CREATE TABLE IF NOT EXISTS `db_region_output_value_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `region` varchar(50) NOT NULL DEFAULT '' COMMENT '区域所在地',
  `value` double NOT NULL DEFAULT '0' COMMENT '总产值（单位：亿元）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='区域总产值---销售收入（单位：亿元）';

-- Dumping data for table nanjing_data.db_region_output_value_nj: ~30 rows (大约)
/*!40000 ALTER TABLE `db_region_output_value_nj` DISABLE KEYS */;
INSERT INTO `db_region_output_value_nj` (`id`, `year`, `region`, `value`) VALUES
	(1, 2015, '江宁区', 3120.73),
	(2, 2016, '江宁区', 3002.7),
	(3, 2017, '江宁区', 3252.28),
	(4, 2015, '鼓楼区', 104.43),
	(5, 2016, '鼓楼区', 117.8),
	(6, 2017, '鼓楼区', 150.22),
	(7, 2015, '栖霞区', 3068.4),
	(8, 2016, '栖霞区', 2392.1),
	(9, 2017, '栖霞区', 2718.91),
	(10, 2015, '秦淮区', 132.74),
	(11, 2016, '秦淮区', 159.2),
	(12, 2017, '秦淮区', 191.91),
	(13, 2015, '玄武区', 31.19),
	(14, 2016, '玄武区', 36.4),
	(15, 2017, '玄武区', 42.28),
	(16, 2015, '溧水区', 1125.44),
	(17, 2016, '溧水区', 1080.9),
	(18, 2017, '溧水区', 1091.25),
	(19, 2015, '高淳区', 938.4),
	(20, 2016, '高淳区', 932.2),
	(21, 2017, '高淳区', 944.43),
	(22, 2015, '建邺区', 2.44),
	(23, 2016, '建邺区', 4.2),
	(24, 2017, '建邺区', 4.25),
	(25, 2015, '六合区', 588.16),
	(26, 2016, '六合区', 1463.3),
	(27, 2017, '六合区', 1681.11),
	(28, 2015, '浦口区', 381.61),
	(29, 2016, '浦口区', 1498.4),
	(30, 2017, '浦口区', 1569.52);
/*!40000 ALTER TABLE `db_region_output_value_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_total_hitech_output_value_nj
DROP TABLE IF EXISTS `db_total_hitech_output_value_nj`;
CREATE TABLE IF NOT EXISTS `db_total_hitech_output_value_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `month` int(11) NOT NULL DEFAULT '0' COMMENT '月',
  `day` int(11) NOT NULL DEFAULT '0' COMMENT '日',
  `totalize` double NOT NULL DEFAULT '0' COMMENT '累计值 单位：亿元',
  `value` double NOT NULL DEFAULT '0' COMMENT '总产值  单位：亿元',
  `rate` double NOT NULL DEFAULT '0' COMMENT '同比增长率%',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='南京市高技术产业总产值（单位：亿元）';

-- Dumping data for table nanjing_data.db_total_hitech_output_value_nj: ~3 rows (大约)
/*!40000 ALTER TABLE `db_total_hitech_output_value_nj` DISABLE KEYS */;
INSERT INTO `db_total_hitech_output_value_nj` (`id`, `year`, `month`, `day`, `totalize`, `value`, `rate`) VALUES
	(1, 2015, 12, 31, 5719.94, 5719.94, -0.9),
	(2, 2016, 12, 31, 3063.39, 3063.39, 2.9),
	(3, 2017, 11, 30, 3012.87, 3286.77, 8.3);
/*!40000 ALTER TABLE `db_total_hitech_output_value_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_total_industrial_output_value
DROP TABLE IF EXISTS `db_total_industrial_output_value`;
CREATE TABLE IF NOT EXISTS `db_total_industrial_output_value` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id：自增长',
  `city` varchar(50) NOT NULL DEFAULT '' COMMENT '城市名',
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `value` double NOT NULL DEFAULT '0' COMMENT '产值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='各市工业总产值（单位：亿元）';

-- Dumping data for table nanjing_data.db_total_industrial_output_value: ~39 rows (大约)
/*!40000 ALTER TABLE `db_total_industrial_output_value` DISABLE KEYS */;
INSERT INTO `db_total_industrial_output_value` (`id`, `city`, `year`, `value`) VALUES
	(1, '南京市', 2015, 13065.8),
	(2, '南京市', 2016, 13026.9),
	(3, '南京市', 2017, 13759.32),
	(4, '无锡市', 2015, 2953.34),
	(5, '无锡市', 2016, 3075.49),
	(6, '无锡市', 2017, 3382.77),
	(7, '徐州市', 2015, 12215.91),
	(8, '徐州市', 2016, 13644.36),
	(9, '徐州市', 2017, 14550),
	(10, '常州市', 2015, 11454.3),
	(11, '常州市', 2016, 12266.9),
	(12, '常州市', 2017, 12888.22),
	(13, '南通市', 2015, 13515.33),
	(14, '南通市', 2016, 14525.72),
	(15, '南通市', 2017, 15023),
	(16, '连云港市', 2015, 5574.55),
	(17, '连云港市', 2016, 6255.81),
	(18, '连云港市', 2017, 6550.17),
	(19, '淮安市', 2015, 6726.73),
	(20, '淮安市', 2016, 7476.27),
	(21, '淮安市', 2017, 7824.77),
	(22, '盐城市', 2015, 8253.62),
	(23, '盐城市', 2016, 9180.84),
	(24, '盐城市', 2017, 8581),
	(25, '扬州市', 2015, 9194.19),
	(26, '扬州市', 2016, 9661.65),
	(27, '扬州市', 2017, 9371),
	(28, '镇江市', 2015, 8781.9),
	(29, '镇江市', 2016, 9066.19),
	(30, '镇江市', 2017, 9918.62),
	(31, '泰州市', 2015, 11063.14),
	(32, '泰州市', 2016, 12170.8),
	(33, '泰州市', 2017, 12602.23),
	(34, '宿迁市', 2015, 3863.32),
	(35, '宿迁市', 2016, 4096.7),
	(36, '宿迁市', 2017, 4344.18),
	(37, '苏州市', 2015, 30546.32),
	(38, '苏州市', 2016, 30679.49),
	(39, '苏州市', 2017, 31956.21);
/*!40000 ALTER TABLE `db_total_industrial_output_value` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_total_profit_nj
DROP TABLE IF EXISTS `db_total_profit_nj`;
CREATE TABLE IF NOT EXISTS `db_total_profit_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `value` double NOT NULL DEFAULT '0' COMMENT '利润值 单位：亿元',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='南京市利润总额（单位：亿元）';

-- Dumping data for table nanjing_data.db_total_profit_nj: ~3 rows (大约)
/*!40000 ALTER TABLE `db_total_profit_nj` DISABLE KEYS */;
INSERT INTO `db_total_profit_nj` (`id`, `year`, `value`) VALUES
	(1, 2015, 741.55),
	(2, 2016, 941.81),
	(3, 2017, 860);
/*!40000 ALTER TABLE `db_total_profit_nj` ENABLE KEYS */;

-- Dumping structure for table nanjing_data.db_total_taxation_nj
DROP TABLE IF EXISTS `db_total_taxation_nj`;
CREATE TABLE IF NOT EXISTS `db_total_taxation_nj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT '0' COMMENT '年份',
  `value` double NOT NULL DEFAULT '0' COMMENT '税金 单位：亿元',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='南京税金总额（单位：亿元）';

-- Dumping data for table nanjing_data.db_total_taxation_nj: ~3 rows (大约)
/*!40000 ALTER TABLE `db_total_taxation_nj` DISABLE KEYS */;
INSERT INTO `db_total_taxation_nj` (`id`, `year`, `value`) VALUES
	(1, 2015, 423.14),
	(2, 2016, 329.91),
	(3, 2017, 329.91);
/*!40000 ALTER TABLE `db_total_taxation_nj` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
